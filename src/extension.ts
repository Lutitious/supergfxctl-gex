declare const global: any, imports: any;
const Main = imports.ui.main;
// @ts-ignore
const Me = imports.misc.extensionUtils.getCurrentExtension();

const { Gio } = imports.gi;

import * as Log from './modules/log';
import * as Panel from './modules/panel';

import { IEnableableModule } from './interfaces/iEnableableModule';

export class Extension implements IEnableableModule {
  public gpuModeIndicator = null;
  public quickToggles = null;

  systemMenu = null;

  constructor() {
    // nothing
  }

  enable() {
    Gio.Resource.load(
      `${Me.path}/resources/org.gnome.Shell.Extensions.supergfxctl-gex.gresource`
    )._register();

    this.systemMenu = Main.panel.statusArea.quickSettings;

    if (typeof this.systemMenu == 'undefined') {
      Log.raw('init', 'system menu is not defined');
      return false;
    }

    this.gpuModeIndicator = new Panel.gpuModeIndicator();
    this.quickToggles = new Panel.gpuModeToggle(this.gpuModeIndicator);

    // must be populated in any case!
    this.gpuModeIndicator._indicator = this.gpuModeIndicator._addIndicator();
    this.gpuModeIndicator._indicator.visible = true;
    this.gpuModeIndicator.quickSettingsItems.push(this.quickToggles);

    this.systemMenu._indicators.remove_child(this.systemMenu._system);
    this.systemMenu._indicators.add_child(this.gpuModeIndicator);
    this.systemMenu._addItems(this.gpuModeIndicator.quickSettingsItems);
    this.systemMenu._indicators.add_child(this.systemMenu._system);
  }

  disable() {
    this.quickToggles.stop();
    this.quickToggles.destroy();
    this.quickToggles = null;

    this.gpuModeIndicator.destroy();
    this.gpuModeIndicator = null;
  }
}

// @ts-ignore
function init() {
  return new Extension();
}
