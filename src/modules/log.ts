declare const log: any;

export function raw(text: string = '', prefix: string = '') {
  log(`supergfxctl-gex: ${prefix} ${text}`);
}

export function info(text: string) {
  raw(text, '[INFO]');
}

export function error(text: string, e: any = null) {
  // raw(text, '[ERROR]', e);
  // @ts-ignore
  logError(e, `supergfxctl-gex: ${text}`);
}

export function warn(text: string) {
  raw(text, '[WARN]');
}

export function debug(text: string) {
  raw(text, '[DEBUG]');
}
