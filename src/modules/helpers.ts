declare const imports: any;
// @ts-ignore
const Me = imports.misc.extensionUtils.getCurrentExtension();
const { GLib } = imports.gi;

// getting shell-version
const Config = imports.misc.config;
const [major] = Config.PACKAGE_VERSION.split('.').map((s: string) => Number(s));

import * as Log from './log';

export function spawnCommandLine(command: string) {
  try {
    GLib.spawn_command_line_async(command);
  } catch (e) {
    Log.error(`Spawning command failed: ${command}`, e);
  }
}

export function getGnomeShellVersion(): number | undefined {
  return major >= 40 ? major : undefined;
}
