// @ts-ignore
const Me = imports.misc.extensionUtils.getCurrentExtension();

export const gfxLabels: string[] = [
  'hybrid',
  'integrated',
  'vfio',
  'egpu',
  'asusmuxdiscreet',
  'none',
];

export const gfxLabelsMenu: string[] = [
  'Hybrid',
  'Integrated',
  'VFIO',
  'eGPU',
  'MUX / dGPU',
  'None',
];

// the states supergfxctl returns for the power state
export const powerLabel: string[] = [
  'active',
  'suspended',
  'off',
  'disabled',
  'active', // when MUX is set to dGPU
  'unknown',
];

export const powerLabelFilename: string[] = [
  'active',
  'suspended',
  'off',
  'off',
  'active',
  'active',
];

export const userAction: string[] = [
  'logout',
  'integrated',
  'asusgpumuxdisable',
  'none',
];
