declare const global: any, imports: any;
//@ts-ignore
const Me = imports.misc.extensionUtils.getCurrentExtension();
const { Gio, GLib } = imports.gi;

import * as Log from './log';

export class File {
  public static DBus(name: string) {
    let file = `${Me.path}/resources/dbus/${name}.xml`;
    try {
      let [_ok, bytes] = GLib.file_get_contents(file);
      if (!_ok) Log.warn(`Couldn't read contents of "${file}"`);
      return _ok ? imports.byteArray.toString(bytes) : null;
    } catch (e) {
      Log.error(`Failed to load "${file}"`, e);
    }
  }
}

/**
 * Get a themed icon, using fallbacks from GSConnect's GResource when necessary.
 *
 * @param {string} name - A themed icon name
 * @return {Gio.Icon} A themed icon
 */
export function getIcon(name) {
  // Setup the desktop icons
  const settings = imports.gi.St.Settings.get();

  // Preload our fallbacks
  const iconPath =
    'resource://org/gnome/Shell/Extensions/supergfxctl-gex/icons/scalable';
  const iconNames = [
    'dgpu-active',
    'dgpu-off',
    'dgpu-suspended',
    'gpu-compute',
    'gpu-compute-active',
    'gpu-asusmuxdiscreet',
    'gpu-asusmuxdiscreet-active',
    'gpu-egpu',
    'gpu-egpu-active',
    'gpu-hybrid',
    'gpu-hybrid-active',
    'gpu-integrated',
    'gpu-integrated-active',
    'gpu-vfio',
    'gpu-vfio-active',
    'reboot',
  ];

  // Check our GResource first
  let _resource = {};

  for (const iconName of iconNames) {
    _resource[iconName] = new Gio.FileIcon({
      file: Gio.File.new_for_uri(`${iconPath}/${iconName}.svg`),
    });
  }

  if (_resource[name] !== undefined) return _resource[name];

  // system fallback
  let _desktop = new imports.gi.Gtk.IconTheme();
  _desktop.set_custom_theme(settings.gtk_icon_theme);
  settings.connect('notify::gtk-icon-theme', (settings_) => {
    _desktop.set_custom_theme(settings_.gtk_icon_theme);
  });

  // Check the desktop icon theme
  if (_desktop.has_icon(name)) return new Gio.ThemedIcon({ name: name });

  // Fallback to hoping it's in the theme somewhere
  return new Gio.ThemedIcon({ name: name });
}
